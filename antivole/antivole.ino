#include <Wire.h>
#include <rgb_lcd.h>

rgb_lcd lcd;
const int colorR = 0;
const int colorG = 0;
const int colorB = 255;
boolean securite=false;
int chrono=0;
int chrono_alarme=0;

void setup() 
{
    pinMode(2, OUTPUT); //LED_sortie_D2
    pinMode(A3, INPUT); //Acceleromètre_A3
    pinMode(A2, INPUT); //Capteur-toucher_entrée_A0
    pinMode(A0, INPUT); //Boutton_entrée_A0
    pinMode(A1, INPUT); //Capteur de température
    
    lcd.begin(16, 2); //initialise le nombre de case par ligne de l'écran LCD
    lcd.setRGB(colorR, colorG, colorB);//Set les couleurs
    
    Serial.begin(9600); //Permet l'affichage sur l'ordinateur
}

void securite_status()
{
   //Activation et désactivation de la sécurité
   if (analogRead(A0)!=0){
    if (securite==false){
      securite=true;
      }else{
        securite=false;
        digitalWrite(2,LOW); 
        }
    }
}

void alarme(){
    if (analogRead(A2)!=0){
      if (securite==true){
        digitalWrite(2,HIGH);
        }else{
        digitalWrite(2,LOW);
        }
    }
}

void alarme2(){
    if (analogRead(A3)!=0){
      if (securite==true){
        digitalWrite(2,HIGH);
        }else{
        digitalWrite(2,LOW);
        }
    }
}

void affichage(){
 lcd.setCursor(0,0);
 if (securite==false){
  lcd.print("Securite: NON");
  }else{
    lcd.print("Securite: OUI");
    }
}

void loop() 
{
  if (millis()-chrono > 500){
    securite_status();
    affichage();
    chrono=millis();
    }
  Serial.print("Millis:");
  Serial.println(millis()/1000);
  Serial.print("Chrono:");
  Serial.println(chrono/1000);
  if (millis()-chrono_alarme > 250){
    alarme();
    chrono_alarme=millis();
    }
}
