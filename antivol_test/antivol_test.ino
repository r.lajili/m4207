#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;
const int colorR = 255; const int colorG = 255; const int colorB = 255;
boolean securite=false;
int minutes;int secondes;


void setup() 
{
    pinMode(2, OUTPUT); //LED_sortie_D2
    pinMode(A2, INPUT); //Capteur-toucher_entrée_A0
    pinMode(A0, INPUT); //Boutton_entrée_A0
    
    lcd.begin(16, 2); //initialise le nombre de case par ligne de l'écran LCD
    lcd.setRGB(colorR, colorG, colorB);
    
    Serial.begin(9600);
    secondes=0;
    minutes=0;
}

void securite_status()
{
   if (securite==false){ //QUAND LA SECURITE EST DESACTIVEE
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Securite: OFF");
    if (analogRead(A0)!=0){
      securite=true;
      }
    digitalWrite(2,LOW);
    }else{ //QUAND LA SECURITE EST ACTIVEE
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Securite: ON");
      if (analogRead(A0)!=0){
        securite=false;
        }
      if (analogRead(A2)!=0){
        while (securite==true){
          infraction();
          }
        }
      }
}

void infraction(){
  for (int i=millis()/1000; i<50; i++){
    digitalWrite(2,HIGH);
    digitalWrite(2,LOW);
    }
  }


void loop() 
{
  secondes=millis()/1000;
  minutes=secondes/60;
  lcd.setCursor(0,0);
  lcd.print(secondes);
  lcd.setCursor(0,1);
  lcd.print(minutes);
  infraction();
}
